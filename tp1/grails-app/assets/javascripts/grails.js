function addClickHandlerForDeletionTo(className) {
  document
    .querySelectorAll(className)
    .forEach(item => {
      item.addEventListener("click", event => {
      event.preventDefault();
      const element = event.currentTarget;

      document
        .querySelector('#dialogButtonConfirm')
        .addEventListener('click', () => {
          fetch(element.href, {
            method: 'DELETE'
          }).then(response => {
            const statusCode = response.status;
            if (statusCode >= 200 && statusCode <= 300) {
              redirectAfterDeletionRequest(element.href)
            } else {
              throw new Error("La supression a echoué")
            }
          });
        });

      $('#dialogModal').modal()

    });
  });
}

function redirectAfterDeletionRequest(href) {
  const array = href.split("/");
  while(array.includes('delete')) {
    array.pop()
  }
  window.location.href = array.join("/")

}

window.addEventListener("DOMContentLoaded", function () {
  addClickHandlerForDeletionTo(".delete-on-click")
});