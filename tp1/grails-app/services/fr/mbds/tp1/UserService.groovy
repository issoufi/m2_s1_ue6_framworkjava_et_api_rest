package fr.mbds.tp1

import grails.gorm.services.Service

interface IUserService {

  User get(Serializable id)

  List<User> list(Map args)

  Long count()

  void delete(Serializable id)

  User save(User user)
}

@Service(User)
abstract class UserService implements IUserService {

  @Override
  void delete(Serializable id) {
    User user = get(id)

    if (user == null) return;

    List<Match> winners = Match.findAllByWinner(user)
    List<Match> loosers = Match.findAllByLooser(user)
    for(Match m : winners) {
      m.delete(flush: true)
    }
    for(Match m : loosers) {
      m.delete(flush: true)
    }

    List<Message> authors = Message.findAllByAuthor(user)
    List<Message> targets = Message.findAllByTarget(user)
    for(Message m : authors) {
      m.delete(flush: true)
    }
    for(Message m : targets) {
      m.delete(flush: true)
    }

    List<UserRole> userRoles = UserRole.findAllByUser(user)
    for(UserRole ur : userRoles) {
      ur.delete(flush: true)
    }

    user.delete(flush: true)
  }
}