package fr.mbds.tp1

import grails.gorm.services.Service

interface IRoleService {

    Role get(Serializable id)

    List<Role> list(Map args)

    Long count()

    void delete(Serializable id)

    Role save(Role role)

    Role create(String authority)

    Role get(String authority)

    User affectAdminRole(User user)
    User affectPlayerRole(User user)
    List<User> affectAdminRole(List<User> users)
    List<User> affectPlayerRole(List<User> users)
}

@Service(Role)
abstract class RoleService implements IRoleService {

    @Override
    Role create(String authority) {
        return save(new Role(authority: authority));
    }

    @Override
    Role get(String authority) {
        return Role.findByAuthority(authority);
    }

    @Override
    List<User> affectAdminRole(List<User> users) {
        for(User u : users) {
            affectAdminRole(u)
        }
        return users
    }

    @Override
    List<User> affectPlayerRole(List<User> users) {
        for(User u : users) {
            affectPlayerRole(u)
        }
        return users
    }


    @Override
    User affectAdminRole(User user) {
        Role roleAdmin = get('ROLE_ADMIN');
        UserRole.create(user, roleAdmin, true)
        return user
    }

    @Override
    User affectPlayerRole(User user) {
        Role roleUser = get('ROLE_USER');
        UserRole.create(user, roleUser, true)

        return user
    }
};