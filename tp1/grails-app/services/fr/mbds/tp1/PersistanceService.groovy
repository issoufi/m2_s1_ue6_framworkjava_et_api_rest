package fr.mbds.tp1

import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import org.apache.commons.lang.NullArgumentException
import java.util.UUID

@Transactional
class PersistanceService {

  GrailsApplication grailsApplication

  def saveImageFile(def imageFile) {
    if (imageFile == null) throw new NullArgumentException("imageFile est null")
    if (imageFile.empty) throw new Exception("imageFile est vide")

    def filePath = grailsApplication.config.getProperty('assets.image.filePath')
    def fileUrl = grailsApplication.config.getProperty('assets.image.fileUrl')
    def fileName = "${UUID.randomUUID().toString()}.${extension(imageFile.getOriginalFilename())}"

    imageFile.transferTo(new File("${filePath}/${fileName}"))
    return "${fileUrl}/${fileName}"
  }

  private static String extension(String fullPath) {
    int dot = fullPath.lastIndexOf(".");
    return fullPath.substring(dot + 1);
  }
}
