package fr.mbds.tp1

import grails.gorm.transactions.Transactional
import org.grails.web.json.JSONObject

import java.nio.charset.Charset

@Transactional
class FakeUserService {
  String baseURL = "https://randomuser.me/api"
  UserService userService

  List<User> createFakeUsers(int numberOfUsers) {
    JSONObject response = this.getJsonData("${this.baseURL}?nat=fr&inc=name,email,login,picture&results=${numberOfUsers}")

    List<User> users = new ArrayList<User>()

    for (Object item : response.getJSONArray("results")) {
      JSONObject user = (JSONObject) item;

      String email = user.getString("email")
      String firstName = user.getJSONObject("name").getString("first")
      String lastName = user.getJSONObject("name").getString("last")
      String username = user.getJSONObject("login").getString("username")
      String password = user.getJSONObject("login").getString("password")
      String picture = user.getJSONObject("picture").getString("large")

      users.add(userService.save(new User(
        firstName: firstName,
        lastName: lastName,
        username: username,
        password: password,
        email: email,
        picture: picture
      )))
    }

    return users;
  }

  private JSONObject getJsonData(String url) {
    URL _url = new URL(url)
    HttpURLConnection con = (HttpURLConnection) _url.openConnection()
    con.setRequestMethod("GET")

    int responseCode = con.getResponseCode()
    println "Status code : ${responseCode}"

    if (responseCode != 200)
      return new JSONObject("{\"results\": []}")

    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()))

    String inputLine

    StringBuffer response = new StringBuffer()
    while ((inputLine = br.readLine()) != null) {
      response.append(inputLine)
    }
    br.close();
    println "Response: ${response.toString()}"
    return new JSONObject(response.toString())
  }
}
