package fr.mbds.tp1

class Match {

  User winner;
  User looser;
  int winnerScore;
  int looserScore;

  static constraints = {
    winner nullable: false
    looser nullable: false
  }
}
