package fr.mbds.tp1

class Message {
  User author;
  User target;
  String message;
  Boolean isReaded = false;

  static constraints = {
    author nullable: false, blank: false
    target nullable: false, blank: false
  }
}
