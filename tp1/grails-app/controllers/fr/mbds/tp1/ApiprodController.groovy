package fr.mbds.tp1

import grails.converters.JSON
import grails.converters.XML
import org.springframework.http.HttpStatus

class ApiprodController {
    UserService userService
    MessageService messageService
    MatchService matchService
    RoleService roleService

    static allowedMethods = [user: ["GET", "POST", "PUT", "DELETE"],
                             users: ["GET", "POST"],
                             message: ["GET", "POST", "PUT", "DELETE"],
                             messages: ["GET", "POST"],
                             match: ["GET", "POST", "PUT", "DELETE"],
                             matchs: ["GET", "POST"]]

    def index() { }

    def user(Long id) {
        println "${request.method} /user/${id}"

        boolean hasError = false
        HttpStatus statusCode = HttpStatus.OK
        User u = null

        switch(request.method) {
            case "GET":
                u = userService.get(id);
                break;
            case "POST":
                u = new User(request.JSON)
                if (!u.validate()) { hasError = true; break; }
                userService.save(u);
                u = roleService.affectPlayerRole(u);
                statusCode = HttpStatus.CREATED
                break;
            case "PUT":
                u = userService.get(id);
                if (u == null) break;
                u.properties = request.JSON
                if (!u.validate()) { hasError = true; break; }
                userService.save(u);
                break;
            case "DELETE":
                userService.delete(id)
                return render(status: HttpStatus.OK, text: "The user ${id} has been removed")
            default:
                hasError = true
                statusCode = HttpStatus.METHOD_NOT_ALLOWED
                break;
        }

        if (hasError) {
            render(status: HttpStatus.BAD_REQUEST, text: "Read the fucking manuel :(")
        } else if (u == null) {
            render(status: HttpStatus.NOT_FOUND, text: "User not found !")
        } else {
            withFormat {
                json { render(status: statusCode, text: u as JSON, contentType: "application/json", encoding: "UTF-8") }
                xml { render(status: statusCode, text: u as XML, contentType: "application/xml", encoding: "UTF-8") }
                '*' { render(status: statusCode, text: u as JSON, contentType: "application/json", encoding: "UTF-8") }
            }
        }
    }

    def users() {
        println "${request.method} /users"

        boolean hasError = false
        HttpStatus statusCode = HttpStatus.OK
        List<User> users = new ArrayList<User>();

        switch(request.method) {
            case "GET":
                users = userService.list(params);
                break;
            case "POST":
                def list = request.JSON

                list.each {
                    User u = new User(it)
                    if (!u.validate()) {
                        hasError = true;
                    } else {
                        userService.save(u);
                        u = roleService.affectPlayerRole(u);
                        users.add(u)
                    }
                }
                statusCode = HttpStatus.CREATED;
                break;
        }

        if (hasError) {
            render(status: HttpStatus.BAD_REQUEST, text: "Read the fucking manuel :(")
        } else {
            withFormat {
              json { render(status: statusCode, text: users as JSON, contentType: "application/json", encoding: "UTF-8") }
              xml { render(status: statusCode, text: users as XML, contentType: "application/xml", encoding: "UTF-8") }
              '*' { render(status: statusCode, text: users as JSON, contentType: "application/json", encoding: "UTF-8") }
            }
        }
    }

    def message(Long id) {
        println "${request.method} /message/${id}"

        boolean hasError = false
        HttpStatus statusCode = HttpStatus.OK
        Message m = null

        switch(request.method) {
            case "GET":
                m = messageService.get(id);
                break;
            case "POST":
                m = new Message(request.JSON)
                if (!m.validate()) { hasError = true; break; }
                messageService.save(m);
                statusCode = HttpStatus.CREATED;
                break;
            case "PUT":
                m = messageService.get(id);
                if (m == null) break;
                m.properties = request.JSON
                if (!m.validate()) { hasError = true; break; }
                messageService.save(m);
                break;
            case "DELETE":
                messageService.delete(id)
                return render(status: HttpStatus.OK, text: "The message ${id} has been removed")
            default:
                hasError = true
        }

        if (hasError) {
            render(status: HttpStatus.BAD_REQUEST, text: "Read the fucking manuel :(")
        } else if (m == null) {
            render(status: HttpStatus.NOT_FOUND, text: "Message not found !")
        } else {
            withFormat {
              json { render(status: statusCode, text: m as JSON, contentType: "application/json", encoding: "UTF-8") }
              xml { render(status: statusCode, text: m as XML, contentType: "application/xml", encoding: "UTF-8") }
              '*' { render(status: statusCode, text: m as JSON, contentType: "application/json", encoding: "UTF-8") }
            }
        }
    }

    def messages() {
      println "${request.method} /messages"

      boolean hasError = false
      HttpStatus statusCode = HttpStatus.OK
      List<Message> messages = new ArrayList<Message>();

      switch (request.method) {
        case "GET":
          messages = messageService.list(params);
          break;
        case "POST":
          def list = request.JSON

          list.each {
            Message m = new Message(it)
            if (!m.validate()) {
              hasError = true;
            } else {
              messageService.save(m);
              messages.add(m)
            }
          }
          statusCode = HttpStatus.CREATED;
          break;
        default:
          statusCode = HttpStatus.METHOD_NOT_ALLOWED
          break;
      }

      if (hasError) {
        render(status: HttpStatus.BAD_REQUEST, text: "Read the fucking manuel :(")
      } else {
        withFormat {
          json {
            render(status: statusCode, text: messages as JSON, contentType: "application/json", encoding: "UTF-8")
          }
          xml { render(status: statusCode, text: messages as XML, contentType: "application/xml", encoding: "UTF-8") }
          '*' {
            render(status: statusCode, text: messages as JSON, contentType: "application/json", encoding: "UTF-8")
          }
        }
      }
    }

    def match(Long id) {
      println "${request.method} /match/${id}"

      boolean hasError = false
      HttpStatus statusCode = HttpStatus.OK
      Match m = null

      switch (request.method) {
        case "GET":
          m = matchService.get(id);
          break;
        case "POST":
          m = new Match(request.JSON)
          if (!m.validate()) {
            hasError = true; break;
          }
          matchService.save(m);
          statusCode = HttpStatus.CREATED;
          break;
        case "PUT":
          m = matchService.get(id);
          if (m == null) break;
          m.properties = request.JSON
          if (!m.validate()) {
            hasError = true; break;
          }
          matchService.save(m);
          break;
        case "DELETE":
          matchService.delete(id)
          return render(status: HttpStatus.OK, text: "The match ${id} has been removed")
        default:
          hasError = true
          statusCode = HttpStatus.METHOD_NOT_ALLOWED
          break;
      }

      if (hasError) {
        render(status: HttpStatus.BAD_REQUEST, text: "Read the fucking manuel :(")
      } else if (m == null) {
        render(status: HttpStatus.NOT_FOUND, text: "Match not found !")
      } else {
        withFormat {
          json { render(status: statusCode, text: m as JSON, contentType: "application/json", encoding: "UTF-8") }
          xml { render(status: statusCode, text: m as XML, contentType: "application/xml", encoding: "UTF-8") }
          '*' { render(status: statusCode, text: m as JSON, contentType: "application/json", encoding: "UTF-8") }
        }
      }
    }

    def matchs() {
      println "${request.method} /matchs"

      boolean hasError = false
      HttpStatus statusCode = HttpStatus.OK
      List<Match> matchs = new ArrayList<Match>();

      switch(request.method) {
          case "GET":
            matchs = matchService.list(params)
              break;
          case "POST":
              def list = request.JSON

              list.each {
                  Match m = new Match(it)
                  if (!m.validate()) {
                      hasError = true;
                  } else {
                      matchService.save(m);
                    matchs.add(m)
                  }
              }
              statusCode = HttpStatus.CREATED;
              break;
          default:
            statusCode = HttpStatus.METHOD_NOT_ALLOWED
            break;
      }

      if (hasError) {
          render(status: HttpStatus.BAD_REQUEST, text: "Read the fucking manuel :(")
      } else {
        withFormat {
          json { render(status: statusCode, text: matchs as JSON, contentType: "application/json", encoding: "UTF-8") }
          xml { render(status: statusCode, text: matchs as XML, contentType: "application/xml", encoding: "UTF-8") }
          '*' { render(status: statusCode, text: matchs as JSON, contentType: "application/json", encoding: "UTF-8") }
        }
      }
    }
}
