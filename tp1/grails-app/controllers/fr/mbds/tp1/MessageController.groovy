package fr.mbds.tp1

import grails.validation.ValidationException
import org.springframework.http.HttpStatus

import static org.springframework.http.HttpStatus.*

class MessageController {

  MessageService messageService

  static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

  def index(Integer max) {
    params.max = Math.min(max ?: 10, 100)
    respond messageService.list(params), model: [messageCount: messageService.count()]
  }

  def show(Long id) {
    Message message = messageService.get(id)

    if (!message.isReaded) {
      message.isReaded = true
      try {
        messageService.save(message)
      } catch (ValidationException e) {
        respond message.errors, view: 'index', model: [messageCount: messageService.count(), hasError: "Impossible de consulter le message"]
        return
      }
    }
    respond message
  }

  def create() {
    [user: User]
  }

  def save(Message message) {
    if (message == null) {
      notFound()
      return
    }

    try {
      messageService.save(message)
    } catch (ValidationException | Exception e) {
      respond(message.errors,  view: 'create', model: [user: User, hasError: e.getMessage()])
      return
    }

    request.withFormat {
      form multipartForm {
        flash.message = "Le message a bien été crée"
        redirect(action: 'index')
      }
      '*' { respond message, [status: CREATED] }
    }
  }

  def edit(Long id) {
    [message: messageService.get(id), user: User]
  }

  def update(Message message) {
    if (message == null) {
      notFound()
      return
    }

    try {
      messageService.save(message)
    } catch (ValidationException e) {
      respond message.errors, view: 'edit'
      return
    }

    request.withFormat {
      form multipartForm {
        flash.message = "Le message à bien été modifié"
        redirect message
      }
      '*' { respond message, [status: OK] }
    }
  }

  def delete(Long id) {
    if (id == null) {
      notFound()
      return
    }

    messageService.delete(id)

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'message.label', default: 'Message'), id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NO_CONTENT }
    }
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'message.label', default: 'Message'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
