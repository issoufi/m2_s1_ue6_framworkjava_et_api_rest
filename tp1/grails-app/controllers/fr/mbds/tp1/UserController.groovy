package fr.mbds.tp1

import grails.validation.ValidationException

import static org.springframework.http.HttpStatus.*

class UserController {

  UserService userService
  RoleService roleService
  PersistanceService persistanceService

  static allowedMethods = [saveuser: "POST", update: "PUT", delete: "DELETE"]

  def index(Integer max) {
    params.max = Math.min(max ?: 12, 100)
    respond userService.list(params), model: [userCount: userService.count()]
  }

  def show(Long id) {
    respond userService.get(id)
  }

  def create() {
  }

  def saveuser() {
    if (params.createUser == null) return;

    def imageFile = request.getFile('picture')

    try {
      params.picture = persistanceService.saveImageFile(imageFile)
    } catch(Exception e) {
      return [hasError: true, message: e.getMessage()]
    }

    User user = new User(params);

    if (!user.validate()) {
      return [hasError: true, message: "Paramètre invalide"]
    }

    try {
      userService.save(user)
      roleService.affectPlayerRole(user)
    } catch (ValidationException e) {
      return [hasError: true, message: e.getMessage()]
    }

    request.withFormat {
      form multipartForm {
        flash.message = "${params.username} a bien été crée"
        redirect user
      }
      '*' { respond user, [status: CREATED] }
    }
  }

  def edit(Long id) {
    respond userService.get(id)
  }

  def update(User user) {
    if (user == null) {
      notFound()
      return
    }

    try {
      userService.save(user)
    } catch (ValidationException e) {
      respond user.errors, view: 'edit'
      return
    }

    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])
        redirect user
      }
      '*' { respond user, [status: OK] }
    }
  }

  def delete(Long id) {
    if (id == null) {
      notFound()
      return
    }

    userService.delete(id)

    render status: NO_CONTENT
  }

  protected void notFound() {
    request.withFormat {
      form multipartForm {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
        redirect action: "index", method: "GET"
      }
      '*' { render status: NOT_FOUND }
    }
  }
}
