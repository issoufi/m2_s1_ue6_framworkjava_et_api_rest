package fr.mbds.tp1

class PurgeJob {

    static triggers = {
      cron name: 'purge', cronExpression: "0 0 23 * * ?" // tous les soirs à 23h
    }

    static group = "message"
    static description = "Cron Trigger : Supprime les messages lus tous les soirs"
    def execute() {
        // execute job
        println "Cron lancé!"
        List<Message> messagesReaded = Message.findAllByIsReaded(true);
        for(Message m : messagesReaded) {
          println "${m.id} à été supprimé"
          m.delete(flush: true)
        }

        println "${messagesReaded.size()} messages ont été supprimés"
    }
}
