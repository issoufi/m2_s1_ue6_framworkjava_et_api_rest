<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main" />
  <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
  <title>Authetification</title>
</head>
<body>
  <div class="auth-bg container-fluid d-flex flex-column align-items-center justify-content-center">
    <div class="card auth-wrapper" style="">
      <h3 class="display-4 text-white auth-title">Connectez-vous</h3>
      <div class="card-body">
        <form
          action="${postUrl ?: '/mbdstp/login/authenticate'}"
          method="POST"
          id="loginForm"
          autocomplete="off">
          <div class="form-group">
            <label for="username">
              <g:message code='springSecurity.login.username.label'/>
            </label>
            <input
              type="text"
              class="form-control"
              name="${usernameParameter ?: 'username'}"
              id="username"
              placeholder="Votre nom d'utilisateur" />

          </div>
          <div class="form-group">
            <label for="password">
              <g:message code='springSecurity.login.password.label'/>
            </label>
            <input
              type="password"
              class="form-control"
              name="${passwordParameter ?: 'password'}"
              id="password"
              aria-describedby="usernameHelp"
              placeholder="Votre mot de passe" />
              <g:if test='${flash.message}'>
                <small
                  id="usernameHelp"
                  class="form-text text-danger">
                  ${flash.message}
                </small>
              </g:if>
          </div>
          <div class="form-group form-check">
            <g:checkBox
              class="form-check-input"
              name="${rememberMeParameter ?: 'remember-me'}"
              id="remember_me"
              value="${hasCookie}"/>
            <label class="form-check-label" for="remember_me">
              <g:message code='springSecurity.login.remember.me.label'/>
            </label>
          </div>
          <button type="submit" id="submit" class="btn btn-primary">${message(code: 'springSecurity.login.button')}</button>
        </form>
      </div>
    </div>
  </div>

  <script>
    (function() {
      document.forms['loginForm'].elements['username'].focus();
    })();
  </script>
</body>
</html>