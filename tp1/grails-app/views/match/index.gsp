<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'match.label', default: 'Match')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>
<body>
<div id="list-match" class="container" role="main">
  <h1>Liste des matchs</h1>
  <g:if test="${flash.message}">
    <div class="alert alert-success alert-dismissible" role="alert">
      ${flash.message}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </g:if>
  <g:if test="${hasError}">
    <div class="alert alert-danger alert-dismissible" role="alert">
      ${hasError}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </g:if>
  <div class="list-group table-match">
    <g:each var="match" in="${matchList}">
      <div class="list-group-item list-group-item-action d-flex justify-content-between">
        <div class="d-flex justify-content-center">
          <div class="text-capitalize text-right">
            <g:link class="h3" resource="${match.winner}" action="show">
              <span class="badge badge-primary text-capitalize">
                ${match.winner.firstName} ${match.winner.lastName}
              </span>
              <img
                style='width:55px'
                class='img-fluid rounded-circle'
                src="${match.winner.picture}"
                alt="${match.winner}"/>
            </g:link>
          </div>
          <div class="text-center h1 table-match-scores">${match.winnerScore} <span class="text-black-50">-</span> ${match.looserScore}</div>
          <div class="text-capitalize text-left">
            <g:link class="h3" resource="${match.looser}" action="show">
              <img
                style='width:55px'
                class='img-fluid rounded-circle'
                src="${match.looser.picture}"
                alt="${match.looser}"/>
              <span class="badge badge-secondary text-capitalize">
                ${match.looser.firstName} ${match.looser.lastName}
              </span>
            </g:link>
          </div>
        </div>
        <small class="h4 d-flex justify-content-end align-items-center">
          <g:link class="btn btn-primary mr-2" resource="${match}" action="show"><i class="fas fa-eye"></i></g:link>
          <g:link class="btn btn-warning mr-2" resource="${match}" action="edit"><i class="fas fa-pencil-alt"></i></g:link>
          <g:link
            resource="${match}"
            action="delete"
            class="delete-on-click btn btn-danger">
            <i class="fas fa-trash-alt"></i>
          </g:link>
        </small>
      </div>
    </g:each>
  </div>

  <nav>
    <ul class="pagination">
      <g:paginateBootsrap total="${matchCount ?: 0}"/>
    </ul>
  </nav>
</div>
</body>
</html>