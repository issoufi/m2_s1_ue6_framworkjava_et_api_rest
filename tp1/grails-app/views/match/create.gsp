<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'match.label', default: 'Match')}" />
        <title>Enregistrer un nouveau match</title>
    </head>
    <body>
        <div id="create-match" class="container" role="main">
            <h1>Enregistrer un nouveau match</h1>
            <g:if test="${flash.message}">
                <div class="alert alert-success alert-dismissible" role="alert">
                    ${flash.message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </g:if>
            <g:if test="${hasError}">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    ${hasError}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </g:if>
            <g:form class="was-validated" action="save" method="POST">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="author">Gagnant</label>
                        <g:select class="form-control text-capitalize"
                                  id="winner"
                                  name="winner.id"
                                  from="${user.list().sort { it.lastName }}"
                                  optionKey="id"/>
                        <div class="invalid-feedback">Selectionner un gagant</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="winnerScore">Score</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-star-half-alt"></i>
                                </span>
                            </div>
                            <input
                              type="number"
                              name="winnerScore"
                              id="winnerScore"
                              min="0"
                              value="0"
                              class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="target">Perdant</label>
                        <g:select class="form-control text-capitalize"
                                  id="looser"
                                  name="looser.id"
                                  from="${user.list().sort { it.lastName }}"
                                  optionKey="id"/>
                        <div class="invalid-feedback">Selectionner un perdant</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="looserScore">Score</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-star-half-alt"></i>
                                </span>
                            </div>
                            <input
                              type="number"
                              name="looserScore"
                              id="looserScore"
                              min="0"
                              value="0"
                              class="form-control">
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Enregister</button>
            </g:form>
        </div>
    </body>
</html>
