<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}"/>
  <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div id="create-message" class="container" role="main">
  <h1>Modifier un message</h1>
  <g:if test="${flash.message}">
    <div class="alert alert-success alert-dismissible" role="alert">
      ${flash.message}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </g:if>
  <g:if test="${hasError}">
    <div class="alert alert-danger alert-dismissible" role="alert">
      ${hasError}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </g:if>
  <g:form class="was-validated" resource="${this.message}"method="PUT">
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="author">Auteur</label>
        <g:select class="form-control text-capitalize"
                  id="author"
                  name="author.id"
                  from="${user.list().sort { it.lastName }}"
                  value="${message.author.id}"
                  optionKey="id"/>
        <div class="invalid-feedback">Selectionner un auteur</div>
      </div>
      <div class="form-group col-md-6">
        <label for="target">Destinataire</label>
        <g:select class="form-control text-capitalize"
                  id="target"
                  name="target.id"
                  from="${user.list().sort { it.lastName }}"
                  value="${message.target.id}"
                  optionKey="id"/>
        <div class="invalid-feedback">Selectionner un auteur</div>
      </div>
    </div>
    <div class="form-group">
      <label for="message">Message</label>
      <textarea
        class="form-control"
        id="message"
        name="message"
        rows="3"
        maxlength="254"
        required>${message.message}</textarea>
    </div>
    <button class="btn btn-primary" type="submit">Modifier</button>
  </g:form>
</div>
</body>
</html>
