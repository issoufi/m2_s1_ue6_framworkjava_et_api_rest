<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}"/>
  <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div id="list-message" class="container" role="main">
  <h1 class="h1">Liste des messages</h1>
  <g:if test="${flash.message}">
    <div class="alert alert-success alert-dismissible" role="alert">
      ${flash.message}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </g:if>
  <g:if test="${hasError}">
    <div class="alert alert-danger alert-dismissible" role="alert">
      ${hasError}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </g:if>
  <div class="list-group">
    <g:each var="message" in="${messageList}">
      <div class="message-container list-group-item list-group-item-action flex-column align-items-start">
        <g:if test="${!message.isReaded}">
          <div class="message-indicator bg-primary"></div>
        </g:if>
        <div class="d-flex w-100 justify-content-between">
          <h5 class="mb-1">
            De <g:link resource="${message.author}" action="show">
              <img
                style='width:25px'
                class='img-fluid rounded-circle'
                src="${message.author.picture}"
                alt="${message.author}"/>
                <span class="badge badge-primary text-capitalize">
                  ${message.author.firstName} ${message.author.lastName.charAt(0)}.
                </span>
            </g:link> à <g:link resource="${message.target}" action="show">
              <img
                style='width:25px'
                class='img-fluid rounded-circle'
                src="${message.target.picture}"
                alt="${message.target}"/>
              <span class="badge badge-secondary text-capitalize">
                ${message.target.firstName} ${message.target.lastName.charAt(0)}.
              </span>
            </g:link>
          </h5>
          <small>29 août</small>
        </div>
        <hr class="border border-light mt-1 mb-3">
        <p class="mb-1 text-black-50">${message.message}</p>
        <small class="h4 d-flex justify-content-end">
          <g:link class="badge badge-pill badge-primary mr-2" resource="${message}" action="show"><i class="fas fa-envelope-open"></i></g:link>
          <g:link class="badge badge-pill badge-warning mr-2" resource="${message}" action="edit"><i class="fas fa-pencil-alt"></i></g:link>
          <g:link
            resource="${message}"
            action="delete"
            class="delete-on-click badge badge-pill badge-danger">
            <i class="fas fa-trash-alt"></i>
          </g:link>
        </small>
      </div>
    </g:each>
  </div>
  <nav class="mt-4">
    <ul class="pagination">
      <g:paginateBootsrap total="${messageCount ?: 0}"/>
    </ul>
  </nav>
</div>
</body>
</html>