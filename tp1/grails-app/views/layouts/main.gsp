<!doctype html>
<html lang="fr" class="no-js">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <title>
    <g:layoutTitle default="Grails"/>
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

  <asset:stylesheet src="application.css"/>

  <g:layoutHead/>
</head>

<body class="main-wrapper">
<header>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

    <a class="navbar-brand" href="${createLink(uri: '/')}">
      <g:img dir="images" file="logo_mbds.png" width="auto" height="30px"/>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">

      <ul class="navbar-nav">
        <sec:ifLoggedIn>
          <g:navItem label="Joueur" faIcon="fa fa-users">
            <g:link class="dropdown-item" controller="user">
              <i class="fas fa-eye"></i>Voir les joueurs
            </g:link>
            <g:link class="dropdown-item" controller="user" action="create">
              <i class="fas fa-plus"></i>Créer un joueur
            </g:link>
          </g:navItem>

          <g:navItem label="Messages" faIcon="fas fa-envelope">
            <g:link class="dropdown-item" controller="message">
              <i class="fas fa-eye"></i>Voir les messages
            </g:link>
            <g:link class="dropdown-item" controller="message" action="create">
              <i class="fas fa-plus"></i>Créer un message
            </g:link>
          </g:navItem>

          <g:navItem label="Matchs" faIcon="fas fa-trophy">
            <g:link class="dropdown-item" controller="match">
              <i class="fas fa-eye"></i>Voir les matchs
            </g:link>
            <g:link class="dropdown-item" controller="match" action="create">
              <i class="fas fa-plus"></i>Enregistrer un match
            </g:link>
          </g:navItem>

          <g:navItemAuthenticatedUser faIcon="fas fa-user-circle" var="u">
            <g:link class="dropdown-item" action="show" resource="${u}">Mon profil</g:link>
            <g:form controller="logout">
              <g:submitButton name="Submit" value="Se deconnecter" class="dropdown-item" />
            </g:form>
          </g:navItemAuthenticatedUser>
        </sec:ifLoggedIn>

        <sec:ifNotLoggedIn>
          <g:link controller="login">
            <button class="btn btn-outline-success my-2 my-sm-0" type="button">Se connecter</button>
          </g:link>
        </sec:ifNotLoggedIn>
      </ul>
    </div>
  </nav>
</header>
<main>
  <g:layoutBody/>
</main>
<footer class="footer" role="contentinfo">
</footer>

<!-- Modal -->
<div
  class="modal fade"
  id="dialogModal"
  tabindex="-1"
  role="dialog"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Attention</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Cette action est ireversible.<br>Voulez-vraiment effetuer cette action ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
        <button type="button" class="btn btn-warning" id="dialogButtonConfirm">Confirmer</button>
      </div>
    </div>
  </div>
</div>

<asset:javascript src="application.js"/>

</body>
</html>
