<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
  <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="create-user" class="container-fluid" role="main">
  <div class="container">
    <h1 class="h1">Créer un joueur</h1>
    <g:if test="${hasError}">
      <div class="alert alert-danger alert-dismissible" role="alert">
        ${hasError}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </g:if>
    <g:if test="${isCreated}">
      <div class="alert alert-success alert-dismissible" role="alert">
        ${message}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </g:if>
    <hr class="my-3"/>
    <g:uploadForm action="saveuser" method="POST" class="was-validated">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="username">Identifiant</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fas fa-user-shield"></i>
              </span>
            </div>
            <input
              type="text"
              name="username"
              id="username"
              class="form-control"
              placeholder="identifiant"
              required />
          </div>

        </div>
        <div class="form-group col-md-6">
          <label for="password">Mot de passe</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fas fa-lock"></i>
              </span>
            </div>
            <input
              type="password"
              name="password"
              id="password"
              class="form-control"
              placeholder="Mot de passe"
              required />
          </div>

        </div>
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="fas fa-envelope"></i>
            </span>
          </div>
          <input
            type="email"
            name="email"
            id="email"
            class="form-control"
            placeholder="Email"
            required />
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="lastName">Nom</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fas fa-id-card"></i>
              </span>
            </div>
            <input
              type="text"
              name="lastName"
              id="lastName"
              class="form-control"
              placeholder="Nom"
              required />
          </div>
        </div>
        <div class="form-group col-md-6">
          <label for="firstName">Prénom</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fas fa-id-card"></i>
              </span>
            </div>
            <input
              type="text"
              name="firstName"
              id="firstName"
              class="form-control"
              placeholder="Prénom"
              required />
          </div>

        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-12">
          <div class="custom-file">
            <input
              type="file"
              name="picture"
              id="picture"
              class="custom-file-input"
              accept="image/png, image/jpeg"
              size="19999"
              lang="fr"
              required />
            <label
              for="picture"
              class="custom-file-label">Choisir une image de profil</label>
          </div>
        </div>
      </div>
      <button class="btn btn-primary" type="submit" name="createUser">Créer</button>
    </g:uploadForm>
  </div>
</div>
</body>
</html>
