<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
      <div id="list-user" class="container-fluid" role="main">
        <div class="container">
          <h1 class="h1">Liste des joueurs</h1>
          <g:if test="${flash.message}">
            <div class="alert alert-success alert-dismissible" role="alert">
              ${flash.message}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </g:if>
          <g:if test="${hasError}">
            <div class="alert alert-danger alert-dismissible" role="alert">
              ${hasError}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </g:if>
          <hr class="my-3"/>
          <div class="card-columns">
            <g:each var="user" in="${userList.sort { it.lastName }}">
              <div class="card">
                <g:if test="${user.picture != null && user.picture != ''}">
                  <img src="${user.picture}" alt="${user.lastName} ${user.firstName}" class="card-img-top"/>
                </g:if>
                <div class="card-body">
                  <h5 class="card-title text-capitalize"><span class="text-muted">${user.lastName}</span> ${user.firstName}</h5>
                  <div class="d-flex justify-content-between flex-wrap">
                    <g:link class="btn btn-primary" action="show" resource="${user}">Voir</g:link>
                    <g:link class="btn btn-warning" action="edit" resource="${user}">Modifier</g:link>
                    <g:link class="btn btn-danger delete-on-click" action="delete" resource="${user}">Supprimer</g:link>
                  </div>
                </div>
              </div>
            </g:each>
          </div>
          <nav>
            <ul class="pagination">
              <g:paginateBootsrap max="12" total="${userCount ?: 0}" />
            </ul>
          </nav>
        </div>
      </div>
    </body>
</html>