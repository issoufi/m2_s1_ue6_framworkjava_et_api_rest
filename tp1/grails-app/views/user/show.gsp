<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div id="show-user" class="container-fluid" role="main">
  <div class="container">
    <h1 class="h1">
      <span class="">Profil de <span class="text-capitalize text-muted">${this.user.lastName} ${this.user.firstName}</span></span>
    </h1>
    <g:if test="${flash.message}">
      <div class="alert alert-success alert-dismissible" role="alert">
        ${flash.message}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </g:if>
    <g:if test="${hasError}">
      <div class="alert alert-danger alert-dismissible" role="alert">
        ${hasError}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </g:if>
    <hr class="my-3"/>
    <div class="row">
      <div class="col-4">
        <div class="card">
          <img class="card-img-top" src="${this.user.picture}" alt="">
          <ul class="list-group list-group-flush">
            <li href="#" class="list-group-item list-group-item-action">
              <p class="card-text"><small class="text-muted">Données personnelles</small></p>
              <div class="row">
                <div class="col">
                  <p class="card-text"><span class="badge badge-secondary">Nom</span></p>
                </div>
                <div class="col">
                  <p class="card-text text-right text-capitalize">${this.user.lastName}</p>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <p class="card-text"><span class="badge badge-secondary">Prénom</span></p>
                </div>
                <div class="col">
                  <p class="card-text text-right text-capitalize">${this.user.firstName}</p>
                </div>
              </div>
            </li>
            <li href="#" class="list-group-item list-group-item-action">
              <p class="card-text"><small class="text-muted">Informations de contact</small></p>
              <div class="row">
                <div class="col">
                  <p class="card-text"><span class="badge badge-secondary">Email</span></p>
                </div>
                <div class="col">
                  <p class="card-text text-right">${this.user.email}</p>
                </div>
              </div>

            </li>
            <li href="#" class="list-group-item list-group-item-action">
              <p class="card-text"><small class="text-muted">Autres informations</small></p>
              <div class="row">
                <div class="col">
                  <p class="card-text"><span class="badge badge-success">Compte activé</span></p>
                </div>
                <div class="col">
                  <p class="card-text text-right">${this.user.enabled ? "Oui" : "Non"}</p>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <p class="card-text"><span class="badge badge-secondary">Compte bloqué</span></p>
                </div>
                <div class="col">
                  <p class="card-text text-right">${this.user.accountLocked ? "Oui" : "Non"}</p>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <p class="card-text"><span class="badge badge-secondary">Mot de passe expiré</span></p>
                </div>
                <div class="col">
                  <p class="card-text text-right">${this.user.passwordExpired ? "Oui" : "Non"}</p>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <p class="card-text"><span class="badge badge-secondary">Compte expiré</span></p>
                </div>
                <div class="col">
                  <p class="card-text text-right">${this.user.accountExpired ? "Oui" : "Non"}</p>
                </div>
              </div>
            </li>
          </ul>
          <div class="card-body">
            <g:link
              class="btn btn-warning card-link"
              action="edit"
              resource="${this.user}">Modifier</g:link>
            <g:link
              class="btn btn-danger card-link delete-on-click"
              action="delete"
              resource="${this.user}">Supprimer</g:link>
          </div>
        </div>
      </div>
      <div class="col">
        <canvas id="matchChart" width="400" height="400"></canvas>
      </div>
    </div>
  </div>
</div>
<script>
  document.addEventListener("DOMContentLoaded", function() {
    var ctx = document.getElementById('matchChart').getContext('2d');

    var color = Chart.helpers.color;
    var barChartData = {
      labels: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet'],
      datasets: [{
        type: 'line',
        label: 'Victoire',
        backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
        borderColor: window.chartColors.red,
        data: [
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor())
        ]
      }, {
        type: 'line',
        label: 'Defaite',
        backgroundColor: color(window.chartColors.green).alpha(0.2).rgbString(),
        borderColor: window.chartColors.green,
        data: [
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor()),
          Math.abs(randomScalingFactor())
        ]
      }]
    };

    // Define a plugin to provide data labels
    Chart.plugins.register({
      afterDatasetsDraw: function(chart) {
        var ctx = chart.ctx;

        chart.data.datasets.forEach(function(dataset, i) {
          var meta = chart.getDatasetMeta(i);
          if (!meta.hidden) {
            meta.data.forEach(function(element, index) {
              // Draw the text in black, with the specified font
              ctx.fillStyle = 'rgb(0, 0, 0)';

              var fontSize = 16;
              var fontStyle = 'normal';
              var fontFamily = 'Helvetica Neue';
              ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

              // Just naively convert to string for now
              var dataString = dataset.data[index].toString();

              // Make sure alignment settings are correct
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';

              var padding = 5;
              var position = element.tooltipPosition();
              ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
            });
          }
        });
      }
    });

    window.myBar = new Chart(ctx, {
      type: 'bar',
      data: barChartData,
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Ratio victoir/defaite'
        },
        scales: {
          xAxes: [{
            gridLines: {
              display:false
            }
          }],
          yAxes: [{
            gridLines: {
              display:false
            }
          }]
        }
      }
    });
  });
</script>
<asset:javascript src="utils.js"/>
</body>
</html>
