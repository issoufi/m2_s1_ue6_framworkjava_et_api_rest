package tp1

import fr.mbds.tp1.*
import grails.converters.JSON

class BootStrap {
  FakeUserService fakeUserService
  UserService userService
  MessageService messageService
  MatchService matchService
  RoleService roleService

  def init = { servletContext ->

    if (User.count() > 0) return;

    roleService.create('ROLE_ADMIN');
    roleService.create('ROLE_USER')

    User adminUser = new User(
        firstName: "Issoufi",
        lastName: "Adam",
        username: 'admin',
        password: 'toor',
        picture: "https://randomuser.me/api/portraits/men/60.jpg",
        email: "issoufi.adam@etu.unice.fr")

    userService.save(adminUser);

    ArrayList<User> users = fakeUserService.createFakeUsers(100);

    roleService.affectAdminRole(adminUser)
    roleService.affectPlayerRole(users)

    def playerUser = users.get(0)
    def playerTwoUser = users.get(1)

    Match match = new Match(
      winner: playerUser,
      looser: playerTwoUser,
      winnerScore: 10,
      looserScore: 1
    );

    matchService.save(match);

    Message m1 = new Message(author: playerUser, target: playerTwoUser, message: 'Yo toi !');
    Message m2 = new Message(author: playerTwoUser, target: playerUser, message: 'Oui et toi :)');

    messageService.save(m1);
    messageService.save(m2);

    //Register Review domain for JSON rendering
  }

  def destroy = {
  }
}
