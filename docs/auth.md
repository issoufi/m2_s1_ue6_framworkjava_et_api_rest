# Authetification

[⬅ Page précédente](main.md)

---

## Se connecter

Rendez-vous sur `http://localhost:8081/mbdstp/login/auth` pour vous connecter sur la plateforme.

Les identifinas administrateur par defaut sont :

Identifiant: `admin`

Mot de passe: `toor`

![page login](images/login.png)

## Se connecter

Une fois connecté, vous pouvez vous deconnecté en cliquant sur image de profile en haut à droite puis sur se deonnceter

![page login](images/logout.png)

---

[⬅ Page précédente](main.md)