# Les points bonus

[⬅ Page précédente](main.md)

---

## Ajout d'image (pas de drag and drop)

J'ai modifié la vue `views/user/create.gsp` pour ajouter la possibilité de sélectionner et de télé verser (upload) une image d'une taille maximale de 19999 octets. Les formats acceptés sont le .png et .jpeg.

Concrètement, j'ai rajouté un `input` de type `file` dans mon formulaire POST.

```javascript
<input
  type="file"
  name="picture"
  accept="image/png, image/jpeg"
  size="19999"
  required />
```

Du coté du UserController, le contrôleur associé a cette vue. J'ai ajouté la méthode (action) `saveuser`. C'est Du côté du UserController, le contrôleur associé a cette vue. J'ai ajouté la méthode (action) `saveuser`. C'est cette action qui reçoit les informations du formulaire et notamment l'image choisie par l'utilisateur. 

Cette action ne fait pas grand chose. En effet, elle délègue la tâche à deux services. Le premier service nommé `PersistanceService`, est chargé de stocker l'image sur un serveur Apache en local et de fournir une url pour pouvoir consulter l'image plus tard. Le deuxième service est chargé de "sauvegarder" l'utilisateur sur la base de données. Si tout se déroule sans encombre. Le UserController redirige la page vers le profil de l'utilisateur crée. Un `code 201` est retourné avec un message de confirmation.

### Le service PersistanceService

Comme dit plus haut, ce service est chargé de stocker des images sur un serveur tel que Apache et de fournir une url pour pouvoir accéder à l'image.

Avant d'être stocké le nom de l'image est modifié. Une chaîne de caractère unique (UUID) est générée et utilisée comme nom de ficher. Cela évite d'écraser une image existante ayant le même nom.

```java
  def saveImageFile(def imageFile) {
    // ...
    def filePath = grailsApplication.config.getProperty('assets.image.filePath')
    def fileUrl = grailsApplication.config.getProperty('assets.image.fileUrl')
    def fileName = "${UUID.randomUUID().toString()}.${extension(imageFile.getOriginalFilename())}"

    imageFile.transferTo(new File("${filePath}/${fileName}"))
    return "${fileUrl}/${fileName}"
  }
```

Pour connaître l'emplacement ou elle doit stocker les images et qu'elle base url utiliser pour accéder aux images, le service se sert de deux variables : `filePath` et `fileUrl`

* la variable `filePath` indique l'emplacement ou les images seront enregistré physiquement. Cet emplacement doit être dans un serveur (Apahce, Nginx, etc) et accable en local.
* la variable `fileUrl` est l'url utilisé pour accéder aux images enregistrées

Vous pouvez modifier ces variables en ouvrant le fichier `/tp1/grails-app/conf/application.yml` et en modifiant la variable `filePath` (ligne 141) et `fileUrl` (ligne 142).

## CRON

Pour purger les messages tous les soirs. J'ai utilisé le plugin quartz.

    compile "org.grails.plugins:quartz:2.0.13"

J'ai ensuite généré un `job` que j'ai adapté pour supprimer tous les massages non lu tous les jours à 23h00.

```java
    static triggers = {
      cron name: 'purge', cronExpression: "0 0 23 * * ?" // tous les soirs à 23h
    }
    // ..
    def execute() {
        // ...
        List<Message> messagesReaded = Message.findAllByIsReaded(true);
        for(Message m : messagesReaded) { m.delete(flush: true) }
        // ...
    }
```

## Sécurité

Pour sécuriser le site web, j'ai utilisé le plugin recommandé par notre professeur de tp :

    compile "org.grails.plugins:spring-security-core:3.2.3"

Ce plugin est très puissant. Grace à lui, j'ai pu rapidement hacher les mots de passe, créer des rôles (admin et user) et de restraindre l'accès aux pages en fonction qu'on soit connecté, un administrateur ou un user.

```java
grails.plugin.springsecurity.userLookup.userDomainClassName = 'fr.mbds.tp1.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'fr.mbds.tp1.UserRole'
grails.plugin.springsecurity.authority.className = 'fr.mbds.tp1.Role'
grails.plugin.springsecurity.requestMap.className = 'fr.mbds.tp1.UserRole'
// ...
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
  // ..
	[pattern: '/user/**',        access: ['ROLE_ADMIN']],
	[pattern: '/message/**',     access: ['ROLE_ADMIN']],
	[pattern: '/match/**',       access: ['ROLE_ADMIN']],
	[pattern: '/dbconsole/**',   access: ['ROLE_ADMIN']],
  // ...
  ]
```
Le plugin offre une interface de connexion par défaut et une route pour se déconnecter. On peut surcharger la page de connexion en utilisant le principe de convention de Grails. Chose que je me suis amusée à faire 😋.

---

[⬅ Page précédente](main.md)