# Utilisateur

[⬅ Page précédente](main.md)

---

## Créer un joueur

Pour créer un joueur, il suffit de cliquer sur jouer puis créer un joueur.

![page login](images/create_user.png)

        ⚠️ L'identifiant du joueur doit être unique

### Les images des joueurs créent ne s'affiche pas correctement ?

Si l'image ne s'affiche pas correctement. C'est parce que l'application web n'a pas réussi à la stocker. Pour que les images s'affichent correctement, il suffit de modifier le chemin utilisé pour faire l'enregistrement.  

Ouvrez le fichier `/tp1/grails-app/conf/application.yml` et modifier la variable `filePath` (ligne 141) et `fileUrl` (ligne 142).

`filePath` indique l'emplacement ou les images seront enregistré physiquement. Cet emplacement doit être dans un serveur (Apahce, Nginx, etc) et accable en local.

`fileUrl` est l'url utilisé pour accéder aux images enregistrées.

## Consulter, modifier et supprimer le profil d'un joueur

Vous pouvez consulter, modifier ou supprimer le profil d'un joueur en allant sur "jouer" sur la bar de menu. Puis, cliquez sur "voir les joueurs". En fin, checrchez un joueur et cliquez sur le bouton voir, modifier ou supprimer.

![page login](images/show_user.png)
![page login](images/user_details.png)

---

[⬅ Page précédente](main.md)