# Doucumentation sur l'utlisation du site web

[⬅ Page précédente](../README.md)

---

## Table de matière

### 0. 😎 [Bonus réalisés](bonus.md) 
  * Upload d'image (pas de drag and drop)
  * CRON
  * Sécurité

### 1. 🔐 [Authentification](auth.md) 
  * Se connecter
  * Se deconnecter

### 2. 👨🏽‍💻 [Les utilisateurs](user.md)
  * Créer un utilisateur
  * Consulter le profile d'un utilisateur
  * Modifier un utilisateur
  * Supprimer un utilisateur
  * Voir la liste des utilisateurs

### 2. 📨 [Les messages](message.md)
  * Créer un message
  * Lire un message
  * Modifier un message
  * Supprimer un message
  * Voir la liste de tous les messsages

### 3. 🏆 [Les matchs](match.md)
  * Créer un match
  * consulter un match
  * Modifier un match
  * Supprimer un match
  * Voir la liste de tous les matchs

---

[⬅ Page précédente](../README.md)