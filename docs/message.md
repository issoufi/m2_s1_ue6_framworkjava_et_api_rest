# Message

[⬅ Page précédente](main.md)

---

## Créer un message

Pour créer un message, il suffit de cliquer sur le menu "message" puis sur "créer un message".

![page login](images/create_message.png)

## Consulter, modifier et supprimer un message

Vous pouvez consulter, modifier ou supprimer un message en allant sur le menu "message" puis sur "voir les messages". Et pour terminer, checrchez un messgae et cliquez sur le bouton voir, modifier ou spprimer.

![page login](images/list_message.png)

---

[⬅ Page précédente](main.md)