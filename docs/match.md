# Match

[⬅ Page précédente](main.md)

---

## Créer un match

Pour créer un message, il suffit de cliquer sur "match" puis sur "enregistrer un message".

![page login](images/create_match.png)

## Consulter, modifier et supprimer un match

Vous pouvez consulter, modifier ou supprimer un match en allant sur le menu "match" puis "voir les matchs". Et en fin, checrchez un match et cliquez sur le bouton voir, modifier ou spprimer.

![page login](images/list_match.png)

---

[⬅ Page précédente](main.md)