## Context
  Dans le cadre d’un **projet** qui consiste à créer une **plateforme de gestion de joueur**. Nous avons implémenté une API REST et réalisé un site web dans le but de répondre à tous les besoins du projet. 

  Dans ce document, vous trouverez toutes les informations nécessaires pour exploiter au mieux le site web et l’API REST. Ces derniers vous permettent de manipuler (consulter, créer, mettre à jour et supprimer) les modèles de données suivantes : 
  - User (joueur ou administrateur)
  - Message
  - Match

## Comment télécharger le projet et le lancer ?

  ```bash
  git clone https://gitlab.com/issoufi/m2_s1_ue6_framworkjava_et_api_rest.git wsrest_adam
  cd wsrest_adam/tp1
  grails run app
  ```
  Une fois le projet lancé, vous pouvez accèder au projet via l'url suivante: 
  [`http://localhost:8081/mbdstp`](http://localhost:8081/mbdstp)


## Site Web
  * [Documentation](/docs/main.md) du site web
  * Demonstration en [vidéo](https://www.youtube.com/) (sera disponible en début de semaine)

## API REST
  * [Documentation](https://documenter.getpostman.com/view/2297877/RWgp1f6n) de l'API
  * Demonstration en [vidéo](https://www.youtube.com/) (sera disponible en début de semaine)

### 🚨 Tests de l'API 🚨
Cliquez sur le bouton "Run in postman" pour récupérer ma collection. Elle contient les tests ainsi que l'ensemble de mes requêtes.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/14601b81d65e685dcc1f)
